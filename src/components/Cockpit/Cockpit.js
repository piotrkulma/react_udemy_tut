import React, {useEffect, useRef, useContext} from 'react'
import styled from 'styled-components'
import './Cockpit.css'
import AuthContext from '../../context/auth-context'

const StyledButton = styled.button`
            background-color: ${props => props.alt ? 'red' : 'green'};
            color: white;
            font: inherit;
            border: 1px solid blue;
            padding: 8px;
            cursor: pointer;
            
            &:hover {
                background-color: ${props => props.alt ? 'salmon' : 'lightgreen'};
                color: black;
            }
`

const cockpit = (props) => {
    const toggleButtonRef = useRef(null);
    const authContext = useContext(AuthContext)
    console.log('Auth coc: ' + authContext.authenticated)

    useEffect(() => {
        console.log('[Cockpit.js] useEffect')
/*        setTimeout(() => {
            alert('saved data to cloud!!!')
        } , 1000)*/
        toggleButtonRef.current.click()

        return () => {
            console.log('[Cockpit.js] Cleanup work in  useEffect')
        }
    }, []);

    useEffect(() => {
        console.log('[Cockpit.js] 2nd useEffect')
        return () => {
            console.log('[Cockpit.js] Cleanup work in 2nd useEffect')
        }
    })

    let classes = []
    if (props.personsLength <= 2) {
        classes.push('red')
    }

    if (props.personsLength <= 1) {
        classes.push('bold')
    }

    return (
        <div>
            <h1>{props.title}</h1>
            <p className={classes.join(' ')}>Paragraph</p>
            <StyledButton
                ref={toggleButtonRef}
                alt={props.showPersons}
                onClick={props.click}>
                Toggle Persons
            </StyledButton>
            <button onClick={authContext.login}>Login</button>
        </div>
    )
}

export default React.memo(cockpit)