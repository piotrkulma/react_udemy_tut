import React, {Component} from 'react';
import './App.css';
import Persons from '../components/Persons/Persons'
import Cockpit from '../components/Cockpit/Cockpit'
import Aux from '../hoc/Auxilliary'
import withClass from '../hoc/withClass'
import AuthContext from '../context/auth-context'


class App extends Component {
    constructor(props) {
        super(props)
        console.log('[App.js] constructor')
    }

    state = {
        persons: [
            {id: '1', name: 'Max', age: 28},
            {id: '2', name: 'Peter', age: 29},
            {id: '3', name: 'John', age: 24}
        ],
        showCockpit: true,
        changeCounter: 0,
        authenticated: false
    }

    static getDerivedStateFromProps(props, state) {
        console.log('[App.js] getDerivedStateFromProps', props)
        return state
    }

    componentDidMount() {
        console.log('[App.js] componentDidMount')
    }

    componentDidUpdate(prevProps, prevState, snapshot) {
        console.log('[App.js] componentDidUpdate')
    }

    shouldComponentUpdate(nextProps, nextState, nextContext) {
        console.log('[App.js] shouldComponentUpdate')
        return true
    }

    nameChangedHandler = (event, id) => {
        const personIndex = this.state.persons.findIndex(p => {
            return p.id === id;
        });

        const person = {...this.state.persons[personIndex]};

        person.name = event.target.value;

        const persons = [...this.state.persons]

        persons[personIndex] = person

        this.setState((prevState, props) => {
            return {
                persons: persons,
                changeCounter: prevState.changeCounter + 1
            }
        });
    }

    deletePersonHandler = (index) => {
        const persons = [...this.state.persons]
        persons.splice(index, 1);
        this.setState({persons: persons})
    }

    togglePersonsHandler = () => {
        const doesShow = this.state.showPersons;
        this.setState({showPersons: !doesShow});
    }

    loginHandler = () => {
        this.setState({authenticated: true})
    }

    render() {
        console.log('[App.js] render')

        let persons = null;

        if (this.state.showPersons === true) {
            persons = <Persons
                persons={this.state.persons}
                clicked={this.deletePersonHandler}
                changed={this.nameChangedHandler}
                isAuthenticated={this.state.authenticated}/>
        }

        return (
            <Aux>
                <button onClick={() => {
                    this.setState({showCockpit: false})
                }}>Remove cockpit
                </button>
                <AuthContext.Provider value={{
                    authenticated: this.state.authenticated,
                    login: this.loginHandler
                }}>
                {this.state.showCockpit === true ?
                    <Cockpit
                        title={this.props.appTitle}
                        personsLength={this.state.persons.length}
                        showPerson={this.state.showPersons}
                        click={this.togglePersonsHandler}
                    /> : null}
                {persons}
                </AuthContext.Provider>
            </Aux>
        );
    }
}


export default withClass(App, "App");
